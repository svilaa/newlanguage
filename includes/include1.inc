#include <<includes/include3.inc>>

procedure include_procedure(integer value){
    integer a;
    a:= value + 5*2;
    print a;
}

integer function operation(integer val1, integer val2){
    integer partial;
    partial := imported_operation(val1, val2);
    return 2*partial;
}