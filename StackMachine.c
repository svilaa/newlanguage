/*************************************************************************** 
Stack Machine 
Author: Anthony A. Aaby
Modified by: Jordi Planes
***************************************************************************/ 
#include <string.h> /* for strdup */ 
#include <stdio.h>
#include <stdlib.h>
#include "StackMachine.h"

#define SM_DEBUG 0

/* OPERATIONS: External Representation */ 
char *op_name[] = {"halt", "set_type", "store", "st_array", "jmp_false", "goto", "call", "ret",
		   "data", "ld_int", "ld_var", "dir_to_val", "ld_string", "ld_var_string", "ld_array",
		   "in_int", "in_array", "in_string", "out", "len_var_string", "len_string", "stack_ar_pc", "top_to_return",
		   "lt", "eq", "ne", "gt", "le", "ge", "add", "sub", "mult", "div" }; 

/* CODE Array */ 
struct instruction code[MAX_MEMORY];

/* RUN-TIME Stack */ 
ArgData stack[MAX_MEMORY];

/*------------------------------------------------------------------------- 
  Registers 
  -------------------------------------------------------------------------*/ 
int pc = 0; 
struct instruction ir; 
int ar = 0; 
int top = -1; 
char ch; 
char* new_string;
int stored_top;

/*========================================================================= 
  Fetch Execute Cycle 
  =========================================================================*/ 
void fetch_execute_cycle() 
{ 
  do { 
    if(SM_DEBUG){
      if(stack[top].type == T_INTEGER){
        printf( "PC = %3d IR.arg = %8d IR.op = %10s AR = %3d Top = %3d,%8d\n", 
	       pc, ir.arg.integer, op_name[ir.op], ar, top, stack[top].integer); 
      }
    }
    /* Fetch */ 
    ir = code[pc++]; 
    //printf("\t\tOP: %s %i\n", op_name[ir.op], pc-1);
    //printf("\tTOP: %i %i\n", top, stack[top].integer);
    /* Execute */ 
    switch (ir.op) { 
    case HALT : printf( "halt\n" ); break; 
    case READ_INT : printf( "Input: " ); 
      scanf( "%d", &stack[ar+ir.arg.integer].integer ); break; 
    case READ_ARRAY : printf("Input: ");
          scanf("%d", &stack[ar+stack[top].integer].integer);
          top--;
          break;
    case READ_STRING : printf("Input string: ");
          stack[ar+ir.arg.integer].string = malloc(sizeof(char)*(100+1));
          scanf("%s", stack[ar+ir.arg.integer].string);
          top--;
          break;
    case OUT : 
          //printf("\tprint type %i\n", stack[top].type);
          switch(stack[top].type){
            case T_INTEGER:
              printf( "Output int: %d\n", stack[top--].integer );
              break;
            case T_STRING:
              printf( "Output string: %s\n", stack[top--].string );
              break;
          }
          break; 
    case STORE :
          //printf("\tstore type %i %i %i\n", stack[ar+ir.arg.integer].type, ir.arg.integer, ar+ir.arg.integer);
          switch(stack[top].type){
            case T_INTEGER:
              stack[ar+ir.arg.integer].integer = stack[top].integer;
              stack[ar+ir.arg.integer].type = T_INTEGER;
              //printf("\tSTORE: %i in %i\n", stack[ar+ir.arg.integer].integer, ar+ir.arg.integer);
              break;
            case T_STRING:
              stack[ar+ir.arg.integer].string = stack[top].string;
              stack[ar+ir.arg.integer].type = T_STRING;
              break;
          }
          top--;
          break;
    case STORE_ARRAY:
          stack[stack[top-1].integer].integer = stack[top].integer;
          top-=2;
          break;
    case DIR_TO_VAL:
          //printf("\tDIR_TO_VAL: %i\n", stack[stack[top].integer].integer);
          stack[top].integer = stack[stack[top].integer].integer;
          break;
    case JMP_FALSE : if ( stack[top--].integer == 0 ) 
	pc = ir.arg.integer; 
      break; 
    case GOTO : pc = ir.arg.integer; break; 
    case STACK_AR_PC:
      stack[++top].integer = ar;
      //printf("AR in top %i\n", top);
      stack[++top].integer = pc;
      stored_top = top;
      //printf("PC in top %i\n", top);
      //printf("\tSTACK_AR_PC: ar %i pc %i top %i\n", ar, pc, top);
      break;
    case CALL : 
      //printf("CALL GOTO: %i\n", ir.arg.integer);
      ar = stored_top+1;
      stack[stored_top].integer = pc;
      pc = ir.arg.integer;
      //printf("\tCALL: ar %i pc %i\n", ar, pc);
      //printf("STORED PC: %i\n", stack[stored_top].integer);
      break;
    case RET : 
      //printf("RET PC: %i in top %i\n", stack[top].integer, top);
      pc = stack[top--].integer;
      //printf("RET AR: %i in top %i\n", stack[top].integer, top);
      ar = stack[top--].integer;
      //printf("\tRET pc: %i ar: %i\n", pc, ar);
      //printf("\tRET STACK: %i top: %i\n", stack[top].integer, top);
      break;
    case TOP_TO_RETURN:
      //printf("TOP TO RETURN: top %i type: %i\n", top, stack[top].type);
      switch(stack[top].type){
        case T_INTEGER:
          stack[ar-3].integer = stack[top].integer;
          stack[ar-3].type = T_INTEGER;
          //printf("RETURN IN %i value %i\n", ar-3, stack[ar-3].integer);
          top--;
          break;
      }
      break;
    case DATA : top = top + ir.arg.integer; /*printf("TOP: %i\n", top);*/ break; 
    case SET_TYPE : stack[top].type = ir.arg.integer; /*printf("\tset type: %i %i\n", ir.arg.type, top);*/ break;
    case LD_INT : stack[++top].integer = ir.arg.integer; stack[top].type=T_INTEGER; /*printf("\tLD_INT\n");*/ break; 
    case LD_ARRAY : stack[++top].integer = ar+ir.arg.integer; stack[top].type=T_INTEGER; break;
    case LD_VAR :
            //printf("\tLD_VAR: %i %i\n", ar+ir.arg.integer, stack[ar+ir.arg.integer].integer);
            stack[++top].integer = stack[ar+ir.arg.integer].integer;
            stack[top].type = T_INTEGER;
        break; 
    case LD_VAR_STRING:
            stack[++top].string = (char*)strdup(stack[ar+ir.arg.integer].string);
            stack[top].type = T_STRING;
        break;
    case LD_STRING:
        stack[++top].string = (char*) strdup(ir.arg.string);
        stack[top].type = T_STRING;
        break;
    case LEN_VAR_STRING:
        stack[++top].integer = strlen(stack[ar+ir.arg.integer].string);
        stack[top].type = T_INTEGER;
        break;
    case LEN_STRING:
        stack[top].integer = strlen(stack[top].string);
        stack[top].type = T_INTEGER;
        break;
    case LT : if ( stack[top-1].integer < stack[top].integer ) 
	stack[--top].integer = 1; 
      else stack[--top].integer = 0; 
      break; 
    case EQ : if ( stack[top-1].integer == stack[top].integer ) 
	stack[--top].integer = 1; 
      else stack[--top].integer = 0; 
      break; 
    case NE : if ( stack[top-1].integer != stack[top].integer ) 
                  stack[--top].integer = 1; 
      else stack[--top].integer = 0; 
      break; 
    case GT : if ( stack[top-1].integer > stack[top].integer ) 
	stack[--top].integer = 1; 
      else stack[--top].integer = 0; 
      break; 
    case LE : if ( stack[top-1].integer <= stack[top].integer )
                    stack[--top].integer = 1;
              else stack[--top].integer = 0;
              break;
    case GE : if ( stack[top-1].integer >= stack[top].integer )
                    stack[--top].integer = 1;
              else stack[--top].integer = 0;
              break;
    case ADD : 
      switch(stack[top].type){
        case T_INTEGER:
            stack[top-1].integer = stack[top-1].integer + stack[top].integer; 
            top--;
            break;
        case T_STRING:
            new_string = (char*)malloc(sizeof(char)*(strlen(stack[top-1].string)+strlen(stack[top].string)+1));
            strcpy(new_string, stack[top-1].string);
            strcat(new_string, stack[top].string);
            stack[top-1].string = new_string;
            top--;
            break;
      }
      //printf("ADD: %i\n", stack[top].integer);
      break; 
    case SUB : stack[top-1].integer = stack[top-1].integer - stack[top].integer; 
      top--; 
      break; 
    case MULT : stack[top-1].integer = stack[top-1].integer * stack[top].integer; 
      top--; 
      break; 
    case DIV : stack[top-1].integer = stack[top-1].integer / stack[top].integer; 
      top--; 
      break; 
    default : printf( "%d Internal Error: Memory Dump\n", ir.op ); 
      break; 
    } 
  } 
  while (ir.op != HALT); 
} 
/*************************** End Stack Machine **************************/ 
