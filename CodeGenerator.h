/*************************************************************************** 
  Code Generator
***************************************************************************/ 

#ifndef __CG_H
#define __CG_H

#include "StackMachine.h"

extern int code_offset;

struct FileCode
{
    int op;
    ArgData arg;
};
typedef struct FileCode FileCode;

int data_location();
void increase_data_location(int num);
void decrease_data_location(int num);
void gen_code_i( enum code_ops operation, int arg );
void gen_code_i_const( enum code_ops operation, int arg, int constant );
void gen_code_i_array( enum code_ops operation, int length );
void gen_code_str( enum code_ops operation, char* string );
void gen_code_str_assign( enum code_ops operation, int address );
void gen_code_general(enum code_ops operation, int type, int integer, int constant, int array);
void gen_code( enum code_ops operation, ArgData* arg );
int reserve_loc();
void back_patch_i( int addr, enum code_ops operation, int arg );
void back_patch( int addr, enum code_ops operation, ArgData* arg );
int gen_label();
void print_code();
void read_bytecode( char *file_name );
void write_bytecode( char *file_name );

#endif
