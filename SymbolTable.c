/*************************************************************************** 
Symbol Table Module 
Author: Anthony A. Aaby
Modified by: Jordi Planes
***************************************************************************/ 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "CodeGenerator.h"
#include "SymbolTable.h"

#define DEBUG 0
#define MAX_SCOPE 100

void send_st_log(char* message){
  if(DEBUG){
    printf("%s\n", message);
  }
}


/*========================================================================= 
DECLARATIONS 
=========================================================================*/ 

int scope_stack[MAX_SCOPE];
int current_scope = 0;

/*------------------------------------------------------------------------- 
SYMBOL TABLE ENTRY 
-------------------------------------------------------------------------*/ 
symrec *identifier; 

/*------------------------------------------------------------------------- 
SYMBOL TABLE 
Implementation: a chain of records. 
------------------------------------------------------------------------*/ 
symrec *sym_table = (symrec *)0; /* The pointer to the Symbol Table */ 

/*======================================================================== 
  Operations: Putsym, Getsym 
  ========================================================================*/ 

int check_param_name(CallInfo* call_info, char* param_name){
  CallInfo* current = call_info;
  while(current->next!=NULL){
    if(strcmp (current->name, param_name) == 0){
      return 1;
    }
    current = current->next;
  }
  return 0;
}

int count_params(char* sym_name){
  CallInfo* current = getsym(sym_name)->call_info;
  int count = 0;
  //printf("Count params\n");
  //printf("NULL: %i\n", current==NULL);
  while(current!=NULL){
    count++;
    current = current->next;
  }
  //printf("%i\n", count);
  return count;
}

symrec * putsym (char *sym_name, int type, int constant, int array, int add_data) 
{ 
  symrec *ptr; 
  ptr = (symrec *) malloc (sizeof(symrec)); 
  ptr->name = (char *) malloc (strlen(sym_name)+1); 
  strcpy (ptr->name,sym_name); 
  if(add_data){
    ptr->offset = data_location(); 
  } else{
    ptr->offset = -1;
  }
  ptr->type = type;
  ptr->constant = constant;
  ptr->array = array;
  ptr->call_info = NULL;
  ptr->next = (struct symrec *)sym_table; 
  sym_table = ptr; 
  if(array>0){
    increase_data_location(array);
  }
  //printf("Put %s in %i\n", ptr->name, ptr->offset);
  scope_stack[current_scope]++;
  return ptr; 
} 

void update_call_address(char *sym_name, int address){
  symrec* current = getsym(sym_name);
  if(current != NULL){
    current->call_address = address;
  }
}

void update_call_info(char *sym_name, CallInfo* call_info){
  symrec* current = getsym(sym_name);
  current->call_info = call_info;
  //printf("FIRST::: %s\n", getsym(sym_name)->call_info->name);
}

symrec * getsym (char *sym_name) 
{ 
  symrec *ptr; 
  for ( ptr = sym_table; ptr != (symrec *) 0; ptr = (symrec *)ptr->next ) 
    if (strcmp (ptr->name,sym_name) == 0){
      //printf("Get %s in %i\n", ptr->name, ptr->offset);
      return ptr; 
    }
  return NULL;
} 

int check_scope_sym (char *sym_name){
  symrec *ptr; 
  int i=0, max=scope_stack[current_scope];
  for ( ptr = sym_table; ptr != (symrec *) 0 && i<max; ptr = (symrec *)ptr->next ){
    //printf("check_scope %s == %s\n", sym_name, ptr->name);
    if (strcmp (ptr->name,sym_name) == 0){
      //printf("%s is in the current scope %i\n", ptr->name, current_scope);
      return 1; 
    }
    i++;
  }
  return 0;
}

void push_scope(){
  current_scope++;
  send_st_log("Push scope");
}

void pop_scope(){
  char message[100];
  sprintf(message, "Scope %i has %i symbols", current_scope, scope_stack[current_scope]);
  send_st_log(message);
  int i;
  symrec *ptr = sym_table;
  symrec *tmp;
  int data_to_pop = 0;
  for(i=0; i<scope_stack[current_scope]; i++){
    if(ptr->offset != -1){
      data_to_pop++;
    }

    if(ptr->array>0){
      data_to_pop += ptr->array;
    }

    tmp = ptr->next;
    free(ptr);
    ptr = tmp;
  }
  sym_table = ptr;
  decrease_data_location(data_to_pop);
  //printf("DATA TO POP %i\n", data_to_pop);
  if(data_to_pop > 0){
    gen_code_i(DATA, -data_to_pop);
  }
  scope_stack[current_scope] = 0;
  current_scope--;
  send_st_log("Pop scope");
}

/************************** End Symbol Table **************************/ 

