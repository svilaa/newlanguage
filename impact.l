/*************************************************************************** 
Scanner for the Simple language 
Author: Anthony A. Aaby
Modified by: Jordi Planes
***************************************************************************/ 

%option nounput
%option noinput
%option noyywrap

%{ 
/*========================================================================= 
C-libraries and Token definitions 
=========================================================================*/ 
#include <string.h> /* for strdup */ 
#include <stdlib.h> /* for atoi */ 
#include "impact.tab.h" /* for token definitions and yylval */ 
#include <unistd.h>

#define MAX 100
#define INDENT 4

char filename[ MAX ];
int stack_deep = 0;
int num_line[100] = {[0 ... 99] = 1};
int num_col = 1;

typedef enum {
    FALSE = 0,
    TRUE = 1
} Bool;

char used_files[ MAX ][ MAX ];
int used_files_index = 0;

void add_used_file(char* file){
  strcpy(used_files[used_files_index], file);
  used_files_index++;
}

Bool is_used(char* file){
  int i;
  for(i=0; i<used_files_index; i++){
    if(strcmp(file, used_files[i]) == 0){
      return TRUE;
    }
  }
  return FALSE;
}

void stack_file(char* filename){
  if(!is_used(filename)){
    add_used_file(filename);
    //printf("%*s" "Move to %s\n", stack_deep*INDENT, " ", filename);
    if(access(filename, F_OK) != -1){
      yyin = fopen(filename, "r");
      //printf("%*s" "Push %s\n", stack_deep*INDENT, " ", filename);
      stack_deep++;
      yypush_buffer_state(yy_create_buffer(yyin, YY_BUF_SIZE));
    } else{
      printf("[==> %s doesn't exists <==]\n", filename);
    }
  } else{
      printf("[==> %s already imported <==]\n", filename);
  }
}
%} 

/*========================================================================= 
TOKEN Definitions 
=========================================================================*/ 
DIGIT [0-9] 
ID [a-z][a-zA-Z0-9_]* 
COMMENT \/\/.*\n

/*========================================================================= 
REGULAR EXPRESSIONS defining the tokens for the Simple language 
=========================================================================*/ 
%% 

":=" {num_col+=yyleng; return(ASSGNOP); } 
"==" {num_col+=yyleng; return(EQUAL); }
"!=" {num_col+=yyleng; return(NOTEQ); }
"<=" {num_col+=yyleng; return(LESSEQ); }
">=" {num_col+=yyleng; return(GREATEQ); }
"++" {num_col+=yyleng; return(PLUSPLUS); }
"--" {num_col+=yyleng; return(MINUSMINUS); }
"{" {num_col+=yyleng; return(SCOPE_START); }
"}" {num_col+=yyleng; return(SCOPE_END); }
\"([a-zA-Z0-9]|\\.|[\ \+\-\*\/])*\" {num_col+=yyleng; yylval.id = (char*) strndup(yytext+1, strlen(yytext)-2); return(STR); }
{DIGIT}+ {num_col+=yyleng; yylval.intval = atoi( yytext ); 
return(NUMBER); } 
do {num_col+=yyleng; return(DO); } 
else {num_col+=yyleng; return(ELSE); } 
end {num_col+=yyleng; return(END); } 
fi {num_col+=yyleng; return(FI); } 
if {num_col+=yyleng; return(IF); } 
in {num_col+=yyleng; return(IN); } 
integer {num_col+=yyleng; return(INTEGER); } 
main {num_col+=yyleng; return(MAIN); }
let {num_col+=yyleng; return(LET); } 
read {num_col+=yyleng; return(READ); } 
skip {num_col+=yyleng; return(SKIP); } 
then {num_col+=yyleng; return(THEN); } 
while {num_col+=yyleng; return(WHILE); } 
print {num_col+=yyleng; return(PRINT); } 
procedure {num_col+=yyleng; return(PROCEDURE); }
function {num_col+=yyleng; return(FUNCTION); }
return {num_col+=yyleng; return(RETURN); }
"#include <<"[a-zA-Z][a-zA-Z0-9\_\.\/]*">>" {
    num_col+=yyleng;
    //printf("FILE %s\n", (char*) strndup(yytext+11, strlen(yytext)-13));
    stack_file( (char*) strndup(yytext+11, strlen(yytext)-13));
}
const {num_col+=yyleng; return(CONST); }
array {num_col+=yyleng; return(ARRAY); }
length {num_col+=yyleng; return(LENGTH); };
string {num_col+=yyleng; return(STRING); };
real {num_col+=yyleng; return(REAL); };
{ID} {num_col+=yyleng; yylval.id = (char *) strdup(yytext); return(IDENTIFIER); } 


[ \t]+ {num_col+=yyleng;} /* eat up whitespace */ 

\n { num_line[stack_deep]++; num_col = 1; }

{COMMENT} { num_line[stack_deep]++; } /* eat up whitespace */ 

<<EOF>> {
    //printf("%*s" "Pop\n", stack_deep*4, " ");
    yypop_buffer_state();
    num_line[stack_deep] = 1;
    num_col = 1;
    stack_deep--;
    if (!YY_CURRENT_BUFFER){
        yyterminate();
    }
}

. { return(yytext[0]);} 

%% 

/************************** End Scanner File *****************************/ 

