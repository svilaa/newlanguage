
TARGET=impact

CFLAGS = -g -Wall -DNDEBUG

.phony: clean all test error

all : $(TARGET) VirtualMachine disassembler

disassembler : disassembler.c StackMachine.c CodeGenerator.c
	$(CC) $(CFLAGS) disassembler.c StackMachine.c CodeGenerator.c -o disassembler -lm

VirtualMachine : StackMachine.c VirtualMachine.c CodeGenerator.c
	$(CC) $(CFLAGS) CodeGenerator.c StackMachine.c VirtualMachine.c -o VirtualMachine -lm

$(TARGET) : $(TARGET).tab.c lex.yy.c StackMachine.c CodeGenerator.c SymbolTable.c
	$(CC) $(CFLAGS) $(TARGET).tab.c lex.yy.c StackMachine.c CodeGenerator.c SymbolTable.c -lm -o $(TARGET) 

$(TARGET).tab.h $(TARGET).tab.c : $(TARGET).y
	bison -dv $(TARGET).y 

lex.yy.c : $(TARGET).l
	flex $(TARGET).l

clean:
	rm -f $(TARGET) VirtualMachine disassembler $(TARGET).tab.c $(TARGET).tab.h lex.yy.c $(TARGET).output *.o *~ *.VirtualMachine *.bc


TESTS = $(wildcard tests/*.imp)
ERRORS = $(wildcard errors/*.error)

test: $(TARGET)
	@echo "#### Begin Test" $(TARGET)
	@for t in $(TESTS); do \
		echo "#### Testing" $$t ; \
		./$(TARGET) $$t `basename $$t .imp`.bc; \
	done
	@echo "#### Ending Test"

error: $(TARGET)
	@for t in $(ERRORS); do \
		./$(TARGET) $$t `basename $$t .error`.bc; \
	done
