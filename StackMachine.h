/*************************************************************************** 
  Stack Machine
***************************************************************************/ 

#ifndef __SM_H
#define __SM_H

#define T_INTEGER 10
#define T_STRING 20
#define T_FLOAT 30
#define T_VOID 80

#define IS_VARIABLE 0
#define IS_ARRAY 1

#define IS_NOT_CONSTANT 0
#define IS_CONSTANT 1

/*========================================================================= 
DECLARATIONS 
=========================================================================*/ 
/* OPERATIONS: Internal Representation */ 
enum code_ops { HALT, SET_TYPE, STORE, STORE_ARRAY, JMP_FALSE, GOTO, CALL, RET,
		DATA, LD_INT, LD_VAR, DIR_TO_VAL, LD_STRING, LD_VAR_STRING, LD_ARRAY,
		READ_INT, READ_ARRAY, READ_STRING, OUT, LEN_VAR_STRING, LEN_STRING, STACK_AR_PC, TOP_TO_RETURN,
		LT, EQ, NE, GT, LE, GE, ADD, SUB, MULT, DIV }; 

struct ArgData {
    int type;
    int constant;
    int array;
    union{
        int integer;
        char* string;
    };
};

typedef struct ArgData ArgData;

struct instruction 
{ 
  enum code_ops op; 
  ArgData arg; 
}; 

#define MAX_MEMORY 999

void fetch_execute_cycle();

extern struct instruction code[ MAX_MEMORY ];
extern char *op_name[];

#endif
