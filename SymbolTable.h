/*------------------------------------------------------------------------- 
SYMBOL TABLE RECORD 
-------------------------------------------------------------------------*/ 

struct CallInfo {
    char *name;
    int type;
    struct CallInfo *next;
};
typedef struct CallInfo CallInfo;

struct symrec 
{ 
  char *name; /* name of symbol */ 
  int type;
  int constant;
  int array;
  int call_address;
  CallInfo* call_info;
  int offset; /* data offset */ 

  struct symrec *next; /* link field */ 
}; 
typedef struct symrec symrec; 

int check_param_name(CallInfo* call_info, char* param_name);
int count_params(char* sym_name);

symrec * getsym (char *sym_name);
symrec * putsym (char *sym_name, int type, int constant, int array, int add_data);
void update_call_address(char *sym_name, int address);
void update_call_info(char *sym_name, CallInfo* call_info);

int check_scope_sym (char *sym_name);
void delsym();
void push_scope();
void pop_scope();