%{
/************************************************************************* 
Compiler for the Simple language 
Author: Anthony A. Aaby
Modified by: Jordi Planes
***************************************************************************/ 
/*========================================================================= 
C Libraries, Symbol Table, Code Generator & other C code 
=========================================================================*/ 
#include <stdio.h> /* For I/O */ 
#include <stdlib.h> /* For malloc here and in symbol table */ 
#include <string.h> /* For strcmp in symbol table */ 
#include "SymbolTable.h" /* Symbol Table */ 
#include "StackMachine.h" /* Stack Machine */ 
#include "CodeGenerator.h" /* Code Generator */ 

#define YYDEBUG 1 /* For Debugging */ 

#define DEBUG 0

#define MAX 100

int yylineno;
int yyerror(char *);
int yylex();

void add_used_file(char* file);
char used_files[ MAX ] [ MAX ];
int stack_deep;
int num_line[100];
int num_col;

// Debug print
void send_log(char* info){
  if(DEBUG){
    printf("%s\n", info);
  }
}

int errors; /* Error Count */ 
char error_message[100];
int constant=0;

CallInfo* call_info;
CallInfo* first_call_info;
int num_params;
int is_param_inside_call;

/*------------------------------------------------------------------------- 
The following support backpatching 
-------------------------------------------------------------------------*/ 
struct lbs /* Labels for data, if and while */ 
{ 
  int for_goto; 
  int for_jmp_false; 
}; 

struct lbs * newlblrec() /* Allocate space for the labels */ 
{ 
   return (struct lbs *) malloc(sizeof(struct lbs)); 
}

/*------------------------------------------------------------------------- 
Install identifier & check if previously defined. 
-------------------------------------------------------------------------*/ 
void install ( char *sym_name, int type, int constant, int array, int add_data ) 
{ 
  int is_in_scope = check_scope_sym(sym_name);
  //printf("%s is in scope %i\n", sym_name, is_in_scope);
  if (is_in_scope == 0){
    putsym (sym_name, type, constant, array, add_data); 
    send_log("Installed");
    send_log(sym_name);
  } else { 
    sprintf( error_message, "%s is already defined\n", sym_name ); 
    yyerror( error_message );
  } 
} 

/*------------------------------------------------------------------------- 
If identifier is defined, generate code 
-------------------------------------------------------------------------*/ 
int context_check( char *sym_name ) 
{ 
  symrec *identifier = getsym( sym_name ); 
  return identifier->offset;
} 

struct lbs * lbs_stack[1000];
int lbs_stack_index = -1;

void push_lbs(struct lbs * current_lbs){
  lbs_stack_index++;
  lbs_stack[lbs_stack_index] = current_lbs;
  send_log("Push lbs");
}

struct lbs * pop_lbs(){
  struct lbs * current_stack = lbs_stack[lbs_stack_index];
  lbs_stack_index--;
  send_log("Pop lbs");
  return current_stack;
}

struct lbs * get_lbs(){
  send_log("Get lbs");
  return lbs_stack[lbs_stack_index];
}

struct lbs * lbs_main;


/*========================================================================= 
SEMANTIC RECORDS 
=========================================================================*/ 
%} 

%union semrec /* The Semantic Records */ 
 { 
   int intval; /* Integer values */ 
   char *id; /* Identifiers */ 
   struct lbs *lbls; /* For backpatching */ 
};

/*========================================================================= 
TOKENS 
=========================================================================*/ 
%start program 
%token <intval> NUMBER /* Simple integer */ 
%token <id> IDENTIFIER STR /* Simple identifier and string */ 
%token <lbls> IF WHILE /* For backpatching labels */ 
%token SKIP THEN ELSE FI DO END SCOPE_START SCOPE_END
%token INTEGER READ PRINT LET IN MAIN PROCEDURE CONST ARRAY LENGTH STRING REAL TO_INTEGER TO_REAL FUNCTION RETURN NUM_LINE
%token ASSGNOP LESSEQ GREATEQ NOTEQ EQUAL PLUSPLUS MINUSMINUS

%type <lbls> other

/*========================================================================= 
OPERATOR PRECEDENCE 
=========================================================================*/ 
%left '-' '+' 
%left '*' '/' 
%right '^' 
%right UNITMINUS

/*========================================================================= 
GRAMMAR RULES for the Simple language 
=========================================================================*/ 

%% 

program : {lbs_main = newlblrec(); lbs_main->for_goto = reserve_loc();}
          calls main END { gen_code_i( HALT, 0 ); send_log("halt"); YYACCEPT; } 

calls : /* empty */
          | call calls

call : call_procedure
     | call_function

main : MAIN {back_patch_i( lbs_main->for_goto, GOTO, gen_label() );}
          scope

call_procedure : PROCEDURE IDENTIFIER {
    install($2, T_VOID, 1, 0, 0);
    update_call_address($2, code_offset);
    call_info = (struct CallInfo*)malloc(sizeof(struct CallInfo)); 
    first_call_info = call_info;
    first_call_info->name = NULL;
    push_scope();
    is_param_inside_call = 1;
} 
'(' params_list ')' SCOPE_START { 
      is_param_inside_call = 0;
      /*symrec* current = getsym($2);
      while(current->call_info != NULL){
        printf("Name: %s, Type: %i\n", current->call_info->name, current->call_info->type);
        current->call_info = current->call_info->next;
      }*/
      if(first_call_info->name!=NULL){
        update_call_info($2, first_call_info);
      }
 } commands scope_end { 
                        gen_code_i(RET, 0);
  }

call_function : INTEGER FUNCTION IDENTIFIER {
    install($3, T_INTEGER, 1, 0, 0);
    update_call_address($3, code_offset);
    call_info = (struct CallInfo*)malloc(sizeof(struct CallInfo)); 
    first_call_info = call_info;
    first_call_info->name = NULL;
    push_scope();
    is_param_inside_call = 1;
} 
'(' params_list ')' SCOPE_START { 
      is_param_inside_call = 0;
      /*symrec* current = getsym($3);
      while(current->call_info != NULL){
        printf("Name: %s, Type: %i\n", current->call_info->name, current->call_info->type);
        current->call_info = current->call_info->next;
      }*/
      if(first_call_info->name!=NULL){
        update_call_info($3, first_call_info);
      }
 } commands
   RETURN exp ';' {
        gen_code_i(TOP_TO_RETURN, 0); 
 }
  scope_end { 
                        gen_code_i(RET, 0);
  }

params_list : /* empty */
            | params_seq INTEGER IDENTIFIER {
                              if(check_param_name(first_call_info, $3) == 0){
                                call_info->name = $3;
                                call_info->type = T_INTEGER;
                                install( $3, T_INTEGER, 0, 0, 1 );
                              } else{
                                sprintf(error_message, "Paramater %s already used", $3);
                                yyerror(error_message);
                              }
            }
            | params_seq STRING IDENTIFIER {
                              if(check_param_name(first_call_info, $3) == 0){
                                call_info->name = $3;
                                call_info->type = T_STRING;
                                install( $3, T_STRING, 0, 0, 1 );
                              } else{
                                sprintf(error_message, "Paramater %s already used", $3);
                                yyerror(error_message);
                              }
            }

params_seq : /* empty */
           | params_seq INTEGER IDENTIFIER ',' {
                              if(check_param_name(first_call_info, $3) == 0){
                                call_info->name = $3;
                                call_info->type = T_INTEGER;
                                call_info->next = (struct CallInfo*)malloc(sizeof(struct CallInfo));
                                call_info = call_info->next;
                                install( $3, T_INTEGER, 0, 0, 1 );
                              } else{
                                sprintf(error_message, "Paramater %s already used", $3);
                                yyerror(error_message);
                              }
           }
            | params_seq STRING IDENTIFIER ',' {
                              if(check_param_name(first_call_info, $3) == 0){
                                call_info->name = $3;
                                call_info->type = T_STRING;
                                call_info->next = (struct CallInfo*)malloc(sizeof(struct CallInfo));
                                call_info = call_info->next;
                                install( $3, T_STRING, 0, 0, 1 );
                              } else{
                                sprintf(error_message, "Paramater %s already used", $3);
                                yyerror(error_message);
                              }
            }

scope_start : SCOPE_START { push_scope(); }
scope_end : SCOPE_END { pop_scope(); }

scope : scope_start commands scope_end

declarations : INTEGER id_seq IDENTIFIER { install( $3, T_INTEGER, constant, 0, 1 );
                                           gen_code_i( DATA, 1 ); 
                                           gen_code_i(SET_TYPE, T_INTEGER); }
    | INTEGER id_seq IDENTIFIER ASSGNOP exp {
                      install($3, T_INTEGER, constant, 0, 1);
                      gen_code_i( SET_TYPE, T_INTEGER);
                      gen_code_i( STORE, context_check( $3 ) );
                      gen_code_i( DATA, 1 );
                        }
; 

id_seq : /* empty */ 
    | id_seq IDENTIFIER ',' { install( $2, T_INTEGER, constant, 0, 1 );
                              gen_code_i( DATA, 1 );
                              gen_code_i(SET_TYPE, T_INTEGER); }
    | id_seq IDENTIFIER ASSGNOP exp ',' {
                      install($2, T_INTEGER, constant, 0, 1);
                      gen_code_i( SET_TYPE, T_INTEGER);
                      gen_code_i( STORE, context_check( $2 ) );
                      gen_code_i( DATA, 1 );
                                        }
; 

array_declarations : ARRAY INTEGER id_seq_array IDENTIFIER '[' NUMBER ']' {
                    if ($6 > 0){
                      install($4, T_INTEGER, 0, $6, 1);
                      gen_code_i( DATA, 1 );
                      gen_code_i( SET_TYPE, T_INTEGER);
                      gen_code_i( DATA, $6 );
                    } else{
                      sprintf(error_message, "The index %i of %s must be greater than 0", $6, $4);
                      yyerror(error_message);
                    }
                                        }

id_seq_array : /* empty */
             | id_seq_array IDENTIFIER '[' NUMBER ']' ',' {
                    if ($4 > 0){
                      install($2, T_INTEGER, 0, $4, 1);
                      gen_code_i( DATA, 1 );
                      gen_code_i( SET_TYPE, T_INTEGER);
                      gen_code_i( DATA, $4 );
                    } else{
                      sprintf(error_message, "The index %i of %s must be greater than 0", $4, $2);
                      yyerror(error_message);
                    }
                                        }

string_declarations : STRING id_seq_string IDENTIFIER { 
                        install($3, T_STRING, constant, 0, 1);
                        gen_code_i(DATA, 1);
                        gen_code_i( SET_TYPE, T_STRING);
                      }
                    | STRING id_seq_string IDENTIFIER ASSGNOP string {
                        install($3, T_STRING, constant, 0, 1);
                        gen_code_i(STORE, context_check($3));
                        gen_code_i( DATA, 1 );
                        gen_code_i( SET_TYPE, T_STRING);
                      }

id_seq_string : /* empty */
              | id_seq_string IDENTIFIER ',' {
                        install($2, T_STRING, 0, 0, 1);
                        gen_code_i(DATA, 1);
                        gen_code_i( SET_TYPE, T_STRING);
                }
              | id_seq_string IDENTIFIER ASSGNOP string ',' {
                        install($2, T_STRING, 0, 0, 1);
                        gen_code_i(STORE, context_check($2));
                        gen_code_i(DATA, 1);
                        gen_code_i( SET_TYPE, T_STRING);
                }

general_declarations : declarations
                     | CONST {constant=1;} declarations {constant=0;}
                     | array_declarations
                     | string_declarations
                     | CONST {constant=1;} string_declarations {constant=0;}

commands : /* empty */ 
    | commands command ';' 
; 

other : /* empty */ {
                     struct lbs * current_lbs = get_lbs();
                     back_patch_i( current_lbs->for_jmp_false, JMP_FALSE, gen_label() );
                     back_patch_i( current_lbs->for_goto, GOTO, gen_label() );
                     send_log("no else");
                     pop_lbs();
                     }
      | ELSE scope_start {
                    struct lbs * current_lbs = get_lbs();
                    back_patch_i( current_lbs->for_jmp_false, JMP_FALSE, gen_label() );
                    send_log("else");
              }
      commands scope_end {
                    struct lbs * current_lbs = get_lbs();
                    back_patch_i( current_lbs->for_goto, GOTO, gen_label() );
                    pop_lbs();
      }
;

command : SKIP 
   | READ IDENTIFIER {
                  symrec* current = getsym($2);
                  if(current!=NULL){
                    if(!current->array){
                      if(!current->constant){
                        switch(current->type){
                          case T_INTEGER:
                            gen_code_i( READ_INT, context_check( $2 ) );
                            break;
                          case T_STRING:
                            gen_code_i(READ_STRING, context_check($2));
                            break;
                        }
                      } else{
                        sprintf(error_message, "%s can't be assigned because it is constant", $2);
                        yyerror(error_message);
                      }
                    } else{
                      sprintf(error_message, "%s can't be assigned because it is an array", $2);
                      yyerror(error_message);
                    }
                  } else{
                    sprintf(error_message, "%s can't be assigned because it doesn't exists", $2);
                    yyerror(error_message);
                  }
                }
   | READ IDENTIFIER '[' exp ']' {
                  symrec* current = getsym($2);
                  if(current!=NULL){
                    if(current->array){
                      gen_code_i(LD_ARRAY, context_check($2)+1);
                      gen_code_i(ADD, 0);
                      gen_code_i(READ_ARRAY, 0);
                    } else{
                      sprintf(error_message, "%s can't be assigned because it is a variable", $2);
                      yyerror(error_message);
                    }
                  } else{
                    sprintf(error_message, "%s can't be assigned because it doesn't exists", $2);
                    yyerror(error_message);
                  }
                }

   | PRINT exp {
                  gen_code_i( OUT, 0 );
               }
   | PRINT string {
                  gen_code_i(OUT, 0);
   }

   | IDENTIFIER ASSGNOP exp {
                  symrec * current = getsym($1);
                  if(current->constant){
                    sprintf(error_message, "%s is constant", current->name);
                    yyerror(error_message);
                  } else{
                    gen_code_i( STORE, context_check( $1 ) );
                    send_log("ASSGNOP");
                  }
               }
   | IDENTIFIER ASSGNOP string {
                  symrec * current = getsym($1);
                  if(current->constant){
                    sprintf(error_message, "%s is constant", current->name);
                    yyerror(error_message);
                  } else{
                    gen_code_i(STORE, context_check($1));
                  }
               }

   | IDENTIFIER '[' exp ']' { gen_code_i(LD_ARRAY, context_check($1)+1); gen_code_i(ADD, 0); } 
     ASSGNOP exp {
                  symrec* current = getsym($1);
                  if(current != NULL){
                    if(current->array){
                      switch(current->type){
                        case T_INTEGER:
                          gen_code_i(STORE_ARRAY, 0);
                          break;
                      }
                    } else{
                      sprintf(error_message, "%s is not an array", current->name);
                      yyerror(error_message);
                    }
                  } else{
                    sprintf(error_message, "%s doesn't exists" , $1);
                    yyerror(error_message);
                  }
               }

   | IF bool_exp scope_start {
                  $1 = (struct lbs *) newlblrec();
                  $1->for_jmp_false = reserve_loc();
                  send_log("if");
                } 
      commands scope_end {
                  $1->for_goto = reserve_loc();
                  push_lbs($1);
                  send_log("then");
                  }
      other {
                  send_log("fi");
                  }

   | WHILE {
                  $1 = (struct lbs *) newlblrec();
                  $1->for_goto = gen_label();
            }
    bool_exp scope_start {
                  $1->for_jmp_false = reserve_loc();
            }
    commands scope_end {
                  gen_code_i( GOTO, $1->for_goto );
                  back_patch_i( $1->for_jmp_false, JMP_FALSE, gen_label() );
                  }

    | IDENTIFIER PLUSPLUS {
                  gen_code_i( LD_VAR, context_check( $1 ) );
                  gen_code_i( LD_INT, 1 );
                  gen_code_i( ADD, 0 );
                  gen_code_i( STORE, context_check( $1 ) );
                }

    | IDENTIFIER MINUSMINUS {
                  gen_code_i( LD_VAR, context_check( $1 ) );
                  gen_code_i( LD_INT, 1 );
                  gen_code_i( SUB, 0 );
                  gen_code_i( STORE, context_check( $1 ) );
                }
    | general_declarations
    | IDENTIFIER '(' {
              symrec* current = getsym($1);
              if(current != NULL){
                if(current->call_address > 0){
                  if(current->type == T_VOID){
                    gen_code_i(STACK_AR_PC, 0);
                    num_params = 0;
                  } else{
                    sprintf(error_message, "%s is a function, it must be a procedure", $1);
                    yyerror(error_message);
                  }
                } else{
                    sprintf(error_message, "%s is not a procedure", $1);
                    yyerror(error_message);
                }
              } else{
                  sprintf(error_message, "The procedure %s is not declared", $1);
                  yyerror(error_message);
              }

    } params ')' {
              //printf("Function %s\n", $1);
              symrec* current = getsym($1);
              if(current!=NULL && current->call_address > 0){
                //printf("Located at %i\n", current->call_address);
                int call_params = count_params($1);
                if(num_params == call_params){
                  gen_code_i(CALL, current->call_address);
                } else{
                  sprintf(error_message, "Procedure %s has %i parameters, but there are %i", current->name, call_params, num_params);
                  yyerror(error_message);
                }
              } else{
                // These errors are notified before
              }
    }
;

params : /* empty*/
       | param exp { num_params++; }
       | param string { num_params++; }

param : /* empty */
      | param exp ',' { num_params++; }
      | param string ',' { num_params++; }

bool_exp : exp '<' exp { gen_code_i( LT, 0 ); } 
   | exp EQUAL exp { gen_code_i( EQ, 0 ); } 
   | exp NOTEQ exp {gen_code_i( NE, 0 ); }
   | exp '>' exp { gen_code_i( GT, 0 ); } 
   | exp LESSEQ exp {gen_code_i( LE, 0 ); }
   | exp GREATEQ exp {gen_code_i( GE, 0 ); }
;

string : STR { //printf("STRING: %s\n", $1);
               gen_code_str(LD_STRING, $1);
             }
       | string '+' string {
          gen_code_i( ADD, 0 );
       }
       | string '+' IDENTIFIER {
          gen_code_i( LD_VAR_STRING, context_check( $3 ) );
          gen_code_i(ADD,0);
       }
       | "((" IDENTIFIER '+' IDENTIFIER "))" {
          gen_code_i( LD_VAR_STRING, context_check( $2 ) );
          gen_code_i( LD_VAR_STRING, context_check( $4 ) );
          gen_code_i(ADD,0);
       }

exp : NUMBER { gen_code_i( LD_INT, $1 ); } 
   | IDENTIFIER { 
        symrec* current = getsym($1);
        if(current != NULL){
              switch(current->type){
                case T_INTEGER:
                  if(is_param_inside_call && current->array > 0){
                    int i;
                    for(i=0; i<current->array; i++){
                      gen_code_i(LD_ARRAY, context_check($1)+1+i);
                      gen_code_i(STORE_ARRAY, 0);
                    }
                  } else{
                    gen_code_i( LD_VAR, context_check( $1 ) ); send_log("ID");
                  }
                  break;
                case T_STRING:
                  gen_code_i( LD_VAR_STRING, context_check( $1 ) ); send_log("ID");
                  break;
              }
        } else{
        sprintf(error_message, "%s is not declared", $1);
        yyerror(error_message);
        }
      } 
   | IDENTIFIER '[' exp ']' {
        if(getsym($1) != NULL){
          gen_code_i(LD_INT, context_check($1)+1);
          gen_code_i(ADD, 0);
          gen_code_i(DIR_TO_VAL, 0);
        } else{
          sprintf(error_message, "%s is not declared", $1);
          yyerror(error_message);
        }
   }
   | exp '+' exp { gen_code_i( ADD, 0 ); } 
   | exp '-' exp { gen_code_i( SUB, 0 ); } 
   | exp '*' exp { gen_code_i( MULT, 0 ); } 
   | exp '/' exp { gen_code_i( DIV, 0 ); } 
   | '(' exp ')' 
   | '-' exp %prec UNITMINUS { 
                   gen_code_i( LD_INT, -1 );
                   gen_code_i ( MULT, 0 );
                 }
   | LENGTH '(' string ')' {
                    gen_code_i(LEN_STRING, 0);
   }
   | LENGTH '(' IDENTIFIER ')' {
                     symrec* current = getsym($3);
                     if(current!=NULL){
                       switch(current->type){
                          case T_INTEGER:
                             if(current->array){
                               gen_code_i(LD_INT, current->array);
                             } else{
                               sprintf(error_message, "%s is not an array", current->name);
                               yyerror(error_message);
                             }
                            break;
                          case T_STRING:
                             gen_code_i(LEN_VAR_STRING, context_check($3));
                            break;
                       }
                     } else{
                       sprintf(error_message, "%s doesn't exists" , $3);
                       yyerror(error_message);
                     }
       }
    | IDENTIFIER '(' {
              symrec* current = getsym($1);
              if(current != NULL){
                if(current->call_address > 0){
                  if(current->type != T_VOID){
                    gen_code_i(DATA, 1);
                    gen_code_i(STACK_AR_PC, 0);
                    num_params = 0;
                  } else{
                    sprintf(error_message, "%s is not a procedure, it must be a function", $1);
                    yyerror(error_message);
                  }
                } else{
                    sprintf(error_message, "%s is not a function", $1);
                    yyerror(error_message);
                }
              } else{
                  sprintf(error_message, "The function %s is not declared", $1);
                  yyerror(error_message);
              }

    } params ')' {
              //printf("Function %s\n", $1);
              symrec* current = getsym($1);
              if(current!=NULL && current->call_address > 0){
                //printf("Located at %i\n", current->call_address);
                int call_params = count_params($1);
                if(num_params == call_params){
                  gen_code_i(CALL, current->call_address);
                } else{
                  sprintf(error_message, "Function %s has %i parameters, but there are %i", current->name, call_params, num_params);
                  yyerror(error_message);
                }
              } else{
                // These errors are notified before
              }
        }
;

%% 

extern struct instruction code[ MAX_MEMORY ];

/*========================================================================= 
MAIN 
=========================================================================*/ 
int main( int argc, char *argv[] ) 
{ 
  extern FILE *yyin; 
  if ( argc < 3 ) {
    printf("usage <input-file> <output-file>\n");
    return -1;
  }
  yyin = fopen( argv[1], "r" ); 
  add_used_file(argv[1]);
  /*yydebug = 1;*/ 
  errors = 0; 
  printf("Impact Compiler\n");
  yyparse (); 
  printf ( "Parse Completed\n" ); 
  if ( errors == 0 ) 
    { 
      //print_code (); 
      //fetch_execute_cycle();
      write_bytecode( argv[2] );
    } 
  return 0;
} 

/*========================================================================= 
YYERROR 
=========================================================================*/ 
int yyerror ( char *s ) /* Called by yyparse on error */ 
{ 
  errors++; 
  printf ("[==> %s. Error detected near line %i column %i in file %s <==]\n", s, num_line[stack_deep], num_col, used_files[stack_deep]); 
  return 0;
} 
/**************************** End Grammar File ***************************/ 

