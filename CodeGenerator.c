/*************************************************************************** 
Code Generator 
Author: Anthony A. Aaby
Modified by: Jordi Planes
***************************************************************************/ 
#include <string.h> /* for strdup */ 
#include <stdio.h>
#include <stdlib.h>
#include "StackMachine.h"
#include "CodeGenerator.h"

# define DEBUG 0

void print_code();

/*------------------------------------------------------------------------- 
Data Segment 
-------------------------------------------------------------------------*/ 
int data_offset = 0; /* Initial offset */ 
int data_location() /* Reserves a data location */ 
{ 
  //printf("Data location %i\n", data_offset+1);
  return data_offset++; 
} 

void increase_data_location(int num){
  data_offset += num;
}

void decrease_data_location(int num){
  data_offset -= num;
}

/*------------------------------------------------------------------------- 
  Code Segment 
  -------------------------------------------------------------------------*/ 
int code_offset = 0; /* Initial offset */ 
int gen_label() /* Returns current offset */ 
{ 
  return code_offset; 
} 

int reserve_loc() /* Reserves a code location */ 
{ 
  return code_offset++; 
} 

/* Generates code at current location */ 

void gen_code_i( enum code_ops operation, int arg ) 
{ 
  code[code_offset].op = operation; 
  code[code_offset].arg.integer = arg; 
  code[code_offset].arg.constant = 0;
  code[code_offset].arg.array = 0;
  code[code_offset++].arg.type = T_INTEGER;

  if(DEBUG){
    printf("code: %s\n", op_name[operation]);
    print_code();
  }
} 

void gen_code_i_const( enum code_ops operation, int arg, int constant ) 
{ 
  code[code_offset].op = operation; 
  code[code_offset].arg.integer = arg; 
  code[code_offset].arg.constant = constant;
  code[code_offset].arg.array = 0;
  code[code_offset++].arg.type = T_INTEGER;

  if(DEBUG){
    printf("code: %s\n", op_name[operation]);
    print_code();
  }
} 

void gen_code_str( enum code_ops operation, char* string ) 
{ 
  code[code_offset].op = operation; 
  code[code_offset].arg.string = string; 
  code[code_offset].arg.constant = 0;
  code[code_offset].arg.array = 0;
  code[code_offset++].arg.type = T_STRING;

  if(DEBUG){
    printf("code: %s\n", op_name[operation]);
    print_code();
  }
} 

void gen_code_str_assign( enum code_ops operation, int address ) 
{ 
  code[code_offset].op = operation; 
  code[code_offset].arg.integer = address; 
  code[code_offset].arg.constant = 0;
  code[code_offset].arg.array = 0;
  code[code_offset++].arg.type = T_STRING;

  if(DEBUG){
    printf("code: %s\n", op_name[operation]);
    print_code();
  }
} 

void gen_code_general(enum code_ops operation, int type, int integer, int constant, int array){
  code[code_offset].op = operation; 
  code[code_offset].arg.integer = integer; 
  code[code_offset].arg.constant = constant;
  code[code_offset].arg.array = array;
  code[code_offset++].arg.type = type;

  if(DEBUG){
    printf("code: %s\n", op_name[operation]);
    print_code();
  }
}

void gen_code( enum code_ops operation, ArgData* arg ) 
{ 
  code[code_offset].op = operation; 
  code[code_offset++].arg = *arg; 
  if(DEBUG){
    printf("code: %s\n", op_name[operation]);
    print_code();
  }
} 

void gen_code_i_array( enum code_ops operation, int arg ) 
{ 
  code[code_offset].op = operation; 
  code[code_offset].arg.integer = arg; 
  code[code_offset].arg.constant = 0;
  code[code_offset].arg.array = 1;
  code[code_offset++].arg.type = T_INTEGER;

  if(DEBUG){
    printf("code: %s\n", op_name[operation]);
    print_code();
  }
} 

/* Generates code at a reserved location */ 
void back_patch_i( int addr, enum code_ops operation, int arg ) 
{ 
  code[addr].op = operation; 
  code[addr].arg.integer = arg; 
  code[addr].arg.constant = 0;
  code[addr].arg.array = 0;
  code[addr].arg.type = T_INTEGER;
} 

/* Generates code at a reserved location */ 
void back_patch( int addr, enum code_ops operation, ArgData* arg ) 
{ 
  code[addr].op = operation; 
  code[addr].arg = *arg; 
} 

/*------------------------------------------------------------------------- 
Print Code to stdio 
-------------------------------------------------------------------------*/ 
void print_code() 
{ 
  int i = 0; 
  while (i < code_offset) { 
    //printf("Type: %i\n", code[i].arg.type);
    switch(code[i].arg.type){
      case T_INTEGER:
        printf("%3d: %-10s%4d\n",i,op_name[(int) code[i].op], code[i].arg.integer ); 
        break;
      case T_STRING:
        printf("%3d: %-10s%s\n",i,op_name[(int) code[i].op], code[i].arg.string ); 
        break;
    }
    i++; 
  } 
} 

/* Reads code from file */

void read_bytecode( char *file_name ) {
  FILE * bytecode_file = fopen( file_name, "r" );
  FileCode file_code;
  int i, length;  

  fread( &code_offset, sizeof( code_offset ), 1, bytecode_file  );
  fread( &data_offset, sizeof( data_offset ), 1, bytecode_file  );
#ifndef NDEBUG
  printf("Offsets: %d %d\n", code_offset, data_offset );
#endif
  int correct = 1;
  for( i = 0; i < MAX_MEMORY && correct != 0; i++ ) {
    fread( &file_code.op, sizeof( int ), 1, bytecode_file );
    fread( &file_code.arg.constant, sizeof( int ), 1, bytecode_file );
    fread( &file_code.arg.type, sizeof( int ), 1, bytecode_file );
    fread( &file_code.arg.array, sizeof( int ), 1, bytecode_file );
    switch(file_code.arg.type){
      case T_INTEGER:
        fread(&file_code.arg.integer, sizeof(int), 1, bytecode_file);
        break;
      case T_STRING:
        fread(&length, sizeof(int), 1, bytecode_file);
        file_code.arg.string = malloc(sizeof(char)*(length+1));
        fread(file_code.arg.string, sizeof(char), length, bytecode_file);
        file_code.arg.string[length+1] = '\0';
        break;
    }
    correct = file_code.arg.type;
    if(correct != 0){
      back_patch(i, file_code.op, &file_code.arg);
    }
  }
  fclose( bytecode_file );
}

/* Writes code to file */

void write_bytecode( char *file_name ) {
  FILE * bytecode_file = fopen( file_name, "w" );
  FileCode file_code;
  int i, length;
  
  //printf("Code offset %i\n", code_offset);
  //printf("Data offset %i\n", data_offset);

  fwrite( &code_offset, sizeof( code_offset ), 1, bytecode_file  );
  fwrite( &data_offset, sizeof( data_offset ), 1, bytecode_file  );
  for( i = 0; i < code_offset; i++ ) {
    //printf("Code %i\n", code[i].op);
    file_code.op = code[ i ].op;
    file_code.arg = code[ i ].arg;
    fwrite( &file_code.op, sizeof( int ), 1, bytecode_file );
    fwrite( &file_code.arg.constant, sizeof( int ), 1, bytecode_file );
    fwrite( &file_code.arg.type, sizeof( int ), 1, bytecode_file );
    fwrite( &file_code.arg.array, sizeof( int ), 1, bytecode_file );
    //printf("%s %i\n", op_name[file_code.op], file_code.arg.type);
    switch(file_code.arg.type){
      case T_INTEGER:
        fwrite( &file_code.arg.integer, sizeof( int ), 1, bytecode_file );
        break;
      case T_STRING:
        length = strlen(file_code.arg.string);
        //printf("length: %i %s\n", length, file_code.arg.string);
        fwrite( &length, sizeof(int), 1, bytecode_file );
        fwrite( file_code.arg.string, sizeof(char), length, bytecode_file );
        break;
      default:
        //printf("WRITE ERROR\n");
        break;
    }
  }
  fclose( bytecode_file );
}

/************************** End Code Generator **************************/ 
